package generalknowledge_p4;

import java.util.Scanner;

public class Generalknowledge_p4 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int decision;
        int n1,n2, n3;
        int dia1,mes1,año1, dia2,mes2,año2,total_dias;
        double r;

        //Bloque de estructura de decision.
        //Se repite mientras que la variable decision sea diferente de cero.
        do {
            //try-catch en caso de que el usuario ingrese algo que no sea numero
            try {
                System.out.println("Elija una opción: "
                        + "\n0 - Salir"
                        + "\n1 - Medir área de un circulo."//resuelto
                        + "\n2 - Medir longitud de un circulo."//resuelto
                        + "\n3 - Saber si dos numeros son iguales."//resuelto
                        + "\n4 - Saber si un numero es positivo o negativo."//resuelto
                        + "\n5 - Saber si los numeros son multiplos."//resuelto
                        + "\n6 - Saber el numero mayor."//resuelto
                        + "\n7 - Ordenar 3 numeros de mayor a menor."//resuelto
                        + "\n8 - Contar cifras de un numero."//resuelto
                        + "\n9 - Saber si la palabra se escribe igual al revés."
                        + "\n10 - Saber numero de días entre dos fechas.");
                decision = sc.nextInt();

                //Bloque de estructura de decision
                //Cada caso acompaña la eleccion tomada en la linea 17
                switch (decision) {
/*                    case x:
                        //codigo aqui
                    break;*/
                    case 1:
                    double radio, area, Longitud;
                    System.out.println("Ingrese el radio");
                    radio=sc.nextDouble();
        
                    area=(Math.PI)*(radio*radio);
                    System.out.println("El area del circulo es: "+area);
                    break;
                    
                    case 2:
                    System.out.println("Ingrese el radio");
                    radio=sc.nextDouble();
                    Longitud=(2)*(Math.PI)*radio;
                    System.out.println("El area del circulo es: "+Longitud);   
                    break;
                    
                    case 3:
                        //codigo aqui
                     System.out.println("Ingrese su primer numero: ");
                     n1= sc.nextInt();
                     System.out.println("ingrese su segundo numero: ");
                     n2= sc.nextInt();
        
                      if(n1==n2){
                     System.out.println("Ambos numeros son iguales ( "+n1+ " es igual a "+n2+" )"); 
                     
                     } else {
                     System.out.println("Ambos numeros son desiguales ( "+n1+ " no es igual a "+n2+ " )");
                     }
                    break;
                    
                    case 4:
                        //codigo aqui
                     System.out.println("Ingrese su  numero: ");
                     n1= sc.nextInt();
      
                     if (n1>0) {
                     System.out.println(" Este numero es positivo: " +n1);
            
                     }else{
                      System.out.println("Este numero es negativo: " +n1);
                      } 
                     
                    break;
                    
                    case 5:   
                    System.out.println("Ingrese un numero");
                    n1=sc.nextInt();
                    System.out.println("Ingrese otro numero");
                    n2=sc.nextInt();
                    
                    if (n1%n2==0) {
                        System.err.println("El numero "+n1+" es multiplo de "+n2);
                        
                    }else if(n1%n2 != 0){
                    System.err.println("El numero "+n1+" No es multiplo de "+n2);
                    
                    }else if(n2%n1 ==0){
                    System.err.println("El numero "+n2+"  es multiplo de "+n1);
                     
                    }else if (n2%n1 != 0){
                        System.out.println("El numero "+n2+" No es multiplo de "+n1);
                    }
            
                    break;
                    
                    case 6:
                         System.out.println("Ingrese un numero");
                    n1=sc.nextInt();
                    System.out.println("Ingrese otro numero");
                    n2=sc.nextInt();  
                    
                    if (n1>n2) {
                        
                        System.out.println("El numero "+n1+" es mayor que el numero "+n2);
                    }else if(n2>n1){
                    
                        System.out.println("El numero "+n2+" es mayor que el numero "+n1);
                    }else{
                    System.out.println("Los numeros son iguales");
                    }
                    break;
                    
                    case 7:
                        System.out.println("A continuación escriba el primer dígito: ");
                        n1 = sc.nextInt();
                        System.out.println("Para continuar escriba el segundo dígito: ");
                        n2 = sc.nextInt();
                        System.out.println("Finalmente escriba el tercer dígito: ");
                        n3 = sc.nextInt();

                        ordenNum(n1, n2, n3);
                        System.out.println("\n\n");
                    break;
                    
                    case 8:
                        System.out.println("Ingrese un numero entre 0 y 9999");
                        n1 = sc.nextInt();
                        
                        String number = String.valueOf(n1);
                        
                        cifrasNum(number);
                        System.out.println("\n\n");
                    break;
                    
                    case 9:
                         String palabra; 
                         System.out.println("Ingrese una palabra: ");
                         palabra = sc.next();
                         palabra = palabra.toLowerCase();
                         int cont =1;
                         
                         for (int i = 0; i < palabra.length(); i++){
                             if(palabra.charAt(i) != palabra.charAt(palabra.length()-1-i)){
                                 cont = 0; break;
                             }
                         }
                         System.out.println( cont == 1 ? "Es un polindromo" : "No es un polindromo");
                         
                      
                        
                         
                    break;
                    
                    case 10:
                        System.out.println("-------Fecha 1--------");
                        System.out.print("Introduzca día: ");
                        dia1 = sc.nextInt();
                        System.out.print("Introduzca mes: ");
                        mes1 = sc.nextInt();
                        System.out.print("Introduzca año: ");
                        año1 = sc.nextInt();
                        System.out.println("-----------Fecha 2-------");
                        System.out.print("Introduzca día: ");
                        dia2 = sc.nextInt();
                        System.out.print("Introduzca mes: ");
                        mes2 = sc.nextInt();
                        System.out.print("Introduzca año: ");
                        año2 = sc.nextInt();
                        // convertimos las dos fechas a días y calculamos la diferencia        
                        total_dias = dia2 - dia1 + 30 * (mes2 - mes1) + 365 * (año2 - año1);
                        System.out.println("Días de diferencia: " + total_dias);
                    break;
                    
                    case 0:
                        System.out.println("\n\n");
                        System.out.println("Thank you... bye.");
                    break;
                    default:
                        System.out.println("Dato incorrecto o no disponible... try again...\n\n");
                    break;
                };
            } catch (Exception e) {
                System.out.println("¡Solo se aceptan numeros!");
                decision = 0;
            }
        } while (decision != 0);
    }
    
    public static void ordenNum(int a, int b, int c) {
        String orden = "";

        if (a >= b && b >= c) {
            orden = "" + a + " " + b + " " + c;
        } else if (b >= a && a >= c) {
            orden = "" + b + " " + a + " " + c;
        } else if (c >= a && a >= b) {
            orden = "" + c + " " + a + " " + b;
        } else if (c >= b && b >= a) {
            orden = "" + c + " " + b + " " + a;
        } else if (a >= c && c >= b) {
            orden = "" + a + " " + c + " " + b;
        } else if (b >= c && c >= a) {
            orden = "" + b + " " + c + " " + a;
        }
        System.out.println("Los numeros ordenados de mayor a menor son: ");
        System.out.println(orden);
    }
    
    public static void cifrasNum(String a){
        System.out.println("El numero contiene " + a.length() + " cifras.");
    }
}
